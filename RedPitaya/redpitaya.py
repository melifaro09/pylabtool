import redpitaya_scpi as scpi
import matplotlib.pyplot as plot

DPINs = ["LED0", "LED1", "LED2", "LED3", "LED4", "LED5", "LED6", "LED7",
         "DIO0_P", "DIO1_P", "DIO2_P", "DIO3_P", "DIO4_P", "DIO5_P", "DIO6_P", "DIO7_P",
         "DIO0_N", "DIO1_N", "DIO2_N", "DIO3_N", "DIO4_N", "DIO5_N", "DIO6_N", "DIO7_N"]

DIRECTIONs = ["OUTP", "INP"]

GENERATORs = ["CH1", "CH2"]

FUNCTIONs = ["SINE", "SQUARE", "TRIANGLE", "SAWU", "SAWD", "PWM", "ARBITRARY"]


class RedPitaya():
    ''' Class to control the RedPitaya board '''
    
    def __init__(self):
        ''' Initialization '''
        print("Initialization finished")
        
    def open(self, address):
        ''' Initialize device '''
        self.__rp = scpi.scpi(address)
        self.__i2c_create()
        
    def pin_list(self):
        return DPINs
    
    def direction_list(self):
        return DIRECTIONs
    
    def values_list(self):
        return ["0", "1"]
    
    def ad5282_list(self):
        return ["#0", "#1", "#2", "#3", "#4", "#5"]
                
    def generators_list(self):
        return GENERATORs
    
    def function_list(self):
        return FUNCTIONs
        
    def start_trig(self, channel):
        self.__rp.tx_txt('ACQ:RST')
        self.__rp.tx_txt('ACQ:START')
        
        if channel == 0:
            self.__rp.tx_txt('ACQ:TRIG CH1_PE')
        else:
            self.__rp.tx_txt('ACQ:TRIG CH2_PE')

        while 1:
            self.__rp.tx_txt('ACQ:TRIG:STAT?')
            if self.__rp.rx_txt() == 'TD':
                break

        self.__rp.tx_txt('ACQ:SOUR1:DATA?')
        buff_string = self.__rp.rx_txt()
        buff_string = buff_string.strip('{}\n\r').replace("  ", "").split(',')
        buff = list(map(float, buff_string))
        
        self.__rp.tx_txt('ACQ:STOP')

        plot.plot(buff)
        plot.ylabel('Voltage')
        plot.show()
        
        return buff
    
    def send_cmd(self, cmd, read_back):
        self.__rp.tx_txt(cmd)
        
        if read_back:
            print(self.__rp.rx_txt())
            #print(self.__rp.rx_arb())
            
    def dpin_direction(self, pin, direction):
        """
        Set direction of the digital pin
        
        Attributes
        --------------
        pin : str
            A pin name ("DIO0_P".."DIO7_P", "DIO0_N".."DIO7_N", "LED0".."LED7")
            or index in DPINs list
            
        direction
            Direction ("OUTP", "INP") or index in DIRECTIONs list
        """
        if type(pin) is int:
            pin = DPINs[pin]
            
        if type(direction) is int:
            direction = DIRECTIONs[direction]
            
        self.__rp.tx_txt('DIG:PIN:DIR {},{}'.format(direction, pin))
        
    def dpin_set(self, pin, value):
        """
        Set state of digital output to HIGH (1) or LOW (0)
        
        Attributes
        --------------
        pin : str
            A pin name ("DIO0_P".."DIO7_P", "DIO0_N".."DIO7_N", "LED0".."LED7")
        value: int
            Output state (0, 1)
        """
        
        if type(pin) is int:
            pin = DPINs[pin]
            
        self.__rp.tx_txt('DIG:PIN {},{}'.format(pin, value))
        
    def trigger(self, ch):
        """
        Start trigger on RF inputs
        
        Attributes
        ---------------
        ch : int
            Channel index [0..1]
        """
        self.__rp.tx_txt('ACQ:RST')
        self.__rp.tx_txt('ACQ:START')
        self.__rp.tx_txt('ACQ:TRIG {}_PE'.format(GENERATORs[ch]))

        while 1:
            self.__rp.tx_txt('ACQ:TRIG:STAT?')
            if self.__rp.rx_txt() == 'TD':
                break

        self.__rp.tx_txt('ACQ:SOUR{}:DATA?'.format(ch + 1))
        buff_string = self.__rp.rx_txt()
        buff_string = buff_string.strip('{}\n\r').replace("  ", "").split(',')
        buff = list(map(float, buff_string))
        
        self.__rp.tx_txt('ACQ:STOP')

        plot.plot(buff)
        plot.ylabel('Voltage')
        plot.show()

    def generator_on(self, output, func, freq, volt):
        """
        Start signal generator
        
        Attributes
        ---------------
        output: int
            Output number ([0..1])
        func: str
            Output function ("SINE", "SQUARE", "TRIANGLE", "SAWU", "SAWD", "PWM", "ARBITRARY")
        freq: int
            Frequency
        volt: float
            Amplitude, V
        """
        if type(func) is int:
            func = FUNCTIONs[func]
            
        output = output + 1
        self.__rp.tx_txt('GEN:RST')
        self.__rp.tx_txt('SOUR{}:FUNC {}'.format(output, func))
        self.__rp.tx_txt('SOUR{}:FREQ:FIX {}'.format(output, freq))
        self.__rp.tx_txt('SOUR{}:VOLT {}'.format(output, volt))
        self.__rp.tx_txt('OUTPUT{}:STATE ON'.format(output))
        
    def generator_off(self, output):
        """
        Stop signal generator
        
        Attributes
        ---------------
        output: int
            Output number ([0..1])
        """
        output = output + 1
        self.__rp.tx_txt('OUTPUT{}:STATE OFF'.format(output))
            
    def __i2c_create(self):
        self.__rp.tx_txt('I2C:RST')
        print(self.__rp.rx_txt())
        
        for i in range(6):
            self.__rp.tx_txt('I2C:CREATE DIO{}_P, DIO{}_N'.format(i, i))
            print(self.__rp.rx_txt())
        
    def i2c_ad5282(self, bus_index, ad_addr, rdac, value):
        self.__rp.tx_txt('I2C:AD5282 {}, {}, {}, {}'.format(bus_index, ad_addr, rdac, value))

    def close(self):
        self.__rp.close()

rp = RedPitaya()
