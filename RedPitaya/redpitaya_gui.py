#from kivy.config import Config
#Config.set('graphics', 'borderless', '1')
#Config.set('graphics', 'fullscreen', 'auto')
#Config.set('graphics', 'show_cursor', '0')
#Config.set('graphics', 'resizable', '0')
#Config.set('graphics', 'allow_screensaver', '0')

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.slider import Slider

from functools import partial

class RedPitayaGUIApp(App):
    """ GUI class """

    def onSliderValueChanged(self, instance, value, i2c_index, ad_addr):
        """ Sliders changing listener """
        pass

    def build(self):
        """ Build window """
        top_box = BoxLayout(orientation='vertical')

        ########################################
        # Labels
        ########################################
        rb_box = BoxLayout(orientation='horizontal', padding=[30, 10, 30, 10])
        top_box.add_widget(rb_box)
        for i in range(6):
            rb_box.add_widget(Label(text="i2c #{}".format(i + 1)))

        ########################################
        # Sliders
        ########################################
        rb_box2 = BoxLayout(orientation='horizontal', padding=[30, 10, 30, 0])
        top_box.add_widget(rb_box2)
        self.__sliders = []
        for i in range(24):
            self.__sliders.append(Slider(min=0, max=255, value=127, orientation='vertical', size_hint=(1, 1.5)))
            rb_box2.add_widget(self.__sliders[i])

        ########################################
        # Labels
        ########################################
        rb_box3 = BoxLayout(orientation='horizontal', padding=[30, 0, 30, 0])
        top_box.add_widget(rb_box3)
        for i in range(6):
            rb_box3.add_widget(Label(text="gg"))
            rb_box3.add_widget(Label(text="++"))

        self.__sliders[0].bind(value=partial(self.onSliderValueChanged, i2c_index=0, ad_addr=0))
        self.__sliders[1].bind(value=partial(self.onSliderValueChanged, i2c_index=0, ad_addr=3))

        return top_box

if __name__ == '__main__':
	RedPitayaGUIApp().run()
