# Python modules for LabTool IDE

The repository contains Python modules to control of various hardware in LabTool IDE laboratory software.

### List of available modules

Module name | Description
------------|-----------------------------------------------------------
MSC         | SmarAct ```MCS-3C``` control system
QWave       | Qwave RGB Photonics Spectrometer
RedPitaya   | RedPitaya communication modul
Steinmeier  | Kollmorgen ```S300``` series moving table
Thorlabs    | K-Cube Auto Aligner ```KPA101```<br>K-Cube Brushless DC Servo Driver ```KBD101```<br>K-Cube brushed DC Servo Motor Controller ```KDC101```
ThorlabsCam | Thorlabs ```DCx (UC480)``` Cameras
uEye        | uEye cameras from IDS Imaging Development Systems GmbH

# Installation

## Requirement

The following python libraries are required:

- PyQt5
- OpenCV
- PythonNet
- pyueye
- pyusb

Installation via pip:

```
pip install PyQt5 pyueye opencv-python pythonnet pyusb
```

# Modules

## MSC

The MSC modul contains functions to control SmartAct's MCS controllers (https://www.smaract.com/control-systems-and-software/product/mcs2). The modul uses ```MSCControl.dll``` and ```SmartActIO.dll``` of SmartAct to interact with a control system.

#### Command list
Command name | Parameters | Description
-------------|------------|-------------------------
Open device  | Device index | Connect to the SmartAct MSC device
Close device | None         | Close connection
List         | None         | List of available devices
Open loop: move step | Channel number<br>Steps number [-30000..30000]<br>Amplitude [0..4095]<br>Frequency [1..18500] | Perform one step for open-loop device
Move absolute | Channel number<br>Position (in nm) | Move close-loop device to absolute position
Move relative | Channel number<br>Relative position (in nm) | Move close-loop device relative to current position

## QWave

The QWave modul implements functions to working with a Photonics Qwave spectrometers. This modul uses ```pyusb``` library to interact with a spectrometer.

#### Command list
Command name         | Parameters | Description
---------------------|------------|-------------------------
Open                 |            |
Close                |            |
Get status           |            |
Get data             |            |
Set exposure         |            |
Set calibration data |            |


## RedPitaya

## Steinmeier

## Thorlabs

## ThorlabsCam

## uEye
The LabTool-modul to working with an uEye cameras from [IDS Imaging Development Systems GmbH](https://en.ids-imaging.com/home.html). The modul needed installation of ```pyueye``` library:

```
pip install pyueye
```

#### Command list

Command name         | Parameters                                            | Description
---------------------|-------------------------------------------------------|-------------------------
Open camera          | Camera index: ```INT```                               | Connect to the uEye camera
Close camera         | -                                                     | Close connection to the uEye camera
Start video          | -                                                     | Start live video
Stop video           | -                                                     | Stop live video
Get image            | -                                                     | Returns image from camera as a NumPy array
Save image           | File name: ```STRING```<br>Insert time: ```BOOLEAN``` | Save image from an uEye camera to the file. If "Insert time" is True, the current date/time will be added to the file name