from ctypes import *

OUTPUT_BUFFER_SIZE		=		256

class MCS():
    ''' Class to control SmartAct MSC controllers '''
    def __init__(self):
        ''' Initialization: loading DLL libraries '''
        self.__dll = WinDLL('MCSControl.dll', use_last_error=True)
        
        out_buf_len = c_uint(OUTPUT_BUFFER_SIZE)
        out_buf = create_string_buffer(OUTPUT_BUFFER_SIZE)
        
        self.__dll.SA_FindSystems(c_char_p(b""), out_buf, byref(out_buf_len))
        
        self.__dev_list = out_buf.value.decode('ascii').split('\n')
        
        for idx, s in enumerate(self.__dev_list):
            print("Founded device {}: {}".format(idx, self.__dev_list[idx]))

        print("MCSControl.dll loaded successfully")
        
    def list(self):
        ''' Returns list of available devices '''
        return self.__dev_list
            
    def open(self, index):
        ''' Initialize device '''
        self.__sys_index = c_void_p()
        print('Conntect to {}...'.format(self.__dev_list[index]))
        self.__dll.SA_OpenSystem(byref(self.__sys_index), c_char_p(self.__dev_list[index].encode()), c_char_p(b"sync"))
        
        self.__ch_number = pointer(c_uint(0))
        self.__dll.SA_GetNumberOfChannels(self.__sys_index, self.__ch_number)
        
        for i in range(0, self.__ch_number[0]):
            self.__dll.SA_SetReportOnComplete_A(self.__sys_index, c_uint(0), 1)
            
        acc = c_uint(100)
        self.__dll.SA_GetClosedLoopMoveAcceleration_S(self.__sys_index, c_uint(0), byref(acc))
        print("Acceleration: " + str(acc))
        
        self.__dll.SA_GetClosedLoopMoveSpeed_S(self.__sys_index, c_uint(0), byref(acc))
        print("Speed: " + str(acc))
        
        print("Channels number: {}\n".format(self.__ch_number[0]))
        
    def move_abs(self, ch, pos):
        ''' Move to absolute position '''
        self.__dll.SA_GotoPositionAbsolute_S(self.__sys_index, c_uint(ch), c_int(pos), c_uint(0))
        
    def move_rel(self, ch, pos):
        self.__dll.SA_GotoPositionRelative_S(self.__sys_index, c_uint(ch), c_int(pos), c_uint(0))
       
    def move_step(self, ch, steps, amplitude = 4095, freq = 1000):
        ''' Move step command for open-loop devices
        
            Arguments:
                ch          -   The channel index (0..ch_num)
                steps       -   Steps number
                amplitude   -   Amplitude
                freq        -   Frequency
        '''
        
        self.__dll.SA_StepMove_S(self.__sys_index, c_uint(ch), c_int(steps), c_int(amplitude), c_uint(freq))

    def close(self):
        ''' Close connection with a device '''
        self.__dll.SA_CloseSystem(self.__sys_index)
        print("MCS device closed")

mcs = MCS()
