import clr

clr.AddReference("uc480DotNet")
clr.AddReference("System")

import uc480
from System import Int32
from System import Double

from uc480.Types import Range

import cv2
import numpy as np

class ThorlabsCamera():
    ''' Class to control Thorlabs KDC101 Kube '''

    def __init__(self):
        ''' Initialization: get device list '''
        pass
    
    def open(self, cam_id):
        ''' Open camera with a given ID '''
        try:
            cam_id = cam_id + 1                             # To start indexes from 0
            self.__camera = uc480.Camera(Int32(cam_id))
            self.__s32MemID = Int32(0)

            ret_status, self.__s32MemID = self.__camera.Memory.Allocate(self.__s32MemID, True)
        
            if ret_status != uc480.Defines.Status.SUCCESS:
                print("Error by allocating memory for the camera")
                return
            
            self.__read_property()

            print("Camera successfully opened!")
        except:
            print("Error opening camera with ID = " + str(cam_id))
            
    def list(self):
        ''' Return list of IDs of the available cameras '''
        c_list = []
        ret_status, c_list = uc480.Info.Camera.GetCameraList(c_list)

        res = []
        for c in c_list:
            res.append(c.SerialNumber)
            
        return res
    
    def full_list(self):
        ''' Return list of ID, Serial number and model name of the available cameras '''
        c_list = []
        ret_status, c_list = uc480.Info.Camera.GetCameraList(c_list)

        res = []
        for c in c_list:
            res.append((c.CameraID, c.SerialNumber, c.Model))
            
        return res
    
    def colormode(self, value):
        mode = 0
        print(self.__camera.PixelFormat.Get(mode))
        #self.__camera.Color.Temperature.RgbModel.Set(uc480.Defines.ColorTemperatureRgbMode.CIE_RGB_E)
    
    def whitebalance(self, enabled, bw):
        self.__camera.AutoFeatures.Software.Whitebalance.SetEnable(enabled)
        
        if bw:
            self.__camera.AutoFeatures.Software.Whitebalance.SetType(uc480.Defines.Whitebalance.Type.GreyWorld)
        else:
            self.__camera.AutoFeatures.Software.Whitebalance.SetType(uc480.Defines.Whitebalance.Type.ColorTemperature)

    def info(self):
        supported = 1
        print(self.__camera.AutoFeatures.Software.Whitebalance.GetSupported(supported))
        print(self.__camera.AutoFeatures.Sensor.Shutter.GetSupported(supported))
        print(self.__camera.Memory.ImageBuffer.GetSupported(supported))
        
    def gain(self, value):
        ''' Set up camera gain value [0..100] '''
        value = 100 if value > 100 else value
        value = 0 if value < 0 else value
        
        self.__camera.AutoFeatures.Software.Gain.SetEnable(True)
        self.__camera.AutoFeatures.Software.Gain.SetMax(value)

    def save(self, image_file):
        ''' Save image from camera to the file '''
        self.__camera.Acquisition.Freeze(uc480.Defines.DeviceParameter.Wait)
        self.__camera.Image.Save(image_file)
        print("Image successfully saved into '" + image_file + "' file")
        
    def __read_property(self):
        ''' Reading image properties into private fields '''
        self.__width   = int(0)             # Image width
        self.__height  = int(0)             # Image height
        self.__bpp     = int(0)             # Bits per pixel
        self.__pitch   = int(0)             # Pitch (?)
        
        ret_result, self.__width, self.__height, self.__bpp, self.__pitch = self.__camera.Memory.Inquire(
                                                                                    self.__s32MemID,
                                                                                    self.__width,
                                                                                    self.__height,
                                                                                    self.__bpp,
                                                                                    self.__pitch)
        
        self.__exp_range = Range[Double]()
        _, self.__exp_range = self.__camera.Timing.Exposure.GetRange(self.__exp_range)
        
        print("Image size: " + str(self.__width) + "x" + str(self.__height))
        print("Bits per pixel: " + str(self.__bpp))
        print("Exposure range: {}-{} ms, step {} ms".format(self.__exp_range.Minimum, self.__exp_range.Maximum, self.__exp_range.Increment))
        
    def exposure(self, value):
        value = int(value / self.__exp_range.Increment) * self.__exp_range.Increment
        value = self.__exp_range.Minimum if value < self.__exp_range.Minimum else value
        value = self.__exp_range.Maximum if value > self.__exp_range.Maximum else value
        
        self.__camera.Timing.Exposure.Set(value)
        print("Set exposure time to {} ms".format(value))
        
    def subsampling(self, factor):
        ''' Set subsampling for the camera '''
        ''' Allowed values for factor: 1, 2, 3, 4, 5, 6, 8, 16 '''
        factor = factor if factor in [1, 2, 3, 4, 5, 6, 8, 16] else 1
        
        factor_to_subsampling_mode = {
                1: (0, 0),
                2: (uc480.Defines.SubsamplingMode.Vertical2X, uc480.Defines.SubsamplingMode.Horizontal2X),
                3: (uc480.Defines.SubsamplingMode.Vertical3X, uc480.Defines.SubsamplingMode.Horizontal3X),
                4: (uc480.Defines.SubsamplingMode.Vertical4X, uc480.Defines.SubsamplingMode.Horizontal4X),
                5: (uc480.Defines.SubsamplingMode.Vertical5X, uc480.Defines.SubsamplingMode.Horizontal5X),
                6: (uc480.Defines.SubsamplingMode.Vertical6X, uc480.Defines.SubsamplingMode.Horizontal6X),
                8: (uc480.Defines.SubsamplingMode.Vertical8X, uc480.Defines.SubsamplingMode.Horizontal8X),
                16: (uc480.Defines.SubsamplingMode.Vertical16X, uc480.Defines.SubsamplingMode.Horizontal16X)
        }
        
        factor_value = factor_to_subsampling_mode[factor]
        self.__camera.Size.Subsampling.Set(factor_value[0] | factor_value[1])
        
    def snap(self):
        self.__camera.Acquisition.Freeze(uc480.Defines.DeviceParameter.Wait)
        
        data = []
        ret_result, data = self.__camera.Memory.CopyToArray(self.__s32MemID, data)

        img = [ int(x) / 255.0 for x in data ]
        
        img = np.array(img).reshape((self.__height, self.__width, 3))

        cv2.imshow("Image shape: " + str(img.shape), img)
        
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
    def close(self):
        ''' Close connection with a device '''
        self.__camera.Exit()
        print("Thorlabs camera closed")

th_cam = ThorlabsCamera()
