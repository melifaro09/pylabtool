import usb.core
import usb.util

QWAVE_VENDOR_ID     =   0x0403
QWAVE_PRODUCT_ID    =   0x90cf

CMD_RET_COMMAND_SUCCESS         =   0
CMD_RET_COMMAND_INVALID         =   1
CMD_RET_PARAMETER_ERROR         =   2
CMD_RET_VALUE_INVALID           =   3
CMD_RET_CODE_INVALID            =   4
CMD_RET_DEVICE_LOCKED           =   5
CMD_RET_FUNCTION_NOT_SUPPORTED  =   6
CMD_RET_COM_TIME_OUT            =   7
CMD_RET_VALUE_NOT_AVAILABLE     =   8
CMD_RET_DEVICE_NOT_RESETTED     =   9

STATUS_IDLE                     =   0
STATUS_WAITINGFORTRIGGER        =   1
STATUS_TAKINGSPECTRUM           =   2
STATUS_HASDATA                  =   3
STATUS_ERROR                    =   -1

ExceptionDict = {CMD_RET_COMMAND_SUCCESS:           "Success",
                 CMD_RET_COMMAND_INVALID:           "Invalid command",
                 CMD_RET_PARAMETER_ERROR:           "Parameter error",
                 CMD_RET_VALUE_INVALID:             "Invalid value",
                 CMD_RET_CODE_INVALID:              "Invalid code",
                 CMD_RET_DEVICE_LOCKED:             "Device is locked",
                 CMD_RET_FUNCTION_NOT_SUPPORTED:    "Function not suppoerted",
                 CMD_RET_COM_TIME_OUT:              "Port timeout",
                 CMD_RET_VALUE_NOT_AVAILABLE:       "Value not available",
                 CMD_RET_DEVICE_NOT_RESETTED:       "Device is not resetted"}

class QWave:
    def __init__(self, verbose = False):
        self.__dev = usb.core.find(idVendor=QWAVE_VENDOR_ID, idProduct=QWAVE_PRODUCT_ID)

        if self.__dev is not None:
            try:
                if self.__dev.is_kernel_driver_active(0):
                    self.__dev.detach_kernel_driver(0) 
            except Exception:
                pass

            self.__dev.set_configuration()
            self.__dev.baudrate = 3000000
            
            self.__cfg = self.__dev.get_active_configuration()
            
            intf = self.__cfg[(0, 0)]
            
            self.__out = usb.util.find_descriptor(intf, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_OUT)
            #self.__out.clear_halt()
            
            self.__in = usb.util.find_descriptor(intf, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_IN)
            #self.__in.clear_halt()
            
            if verbose:
                print("OUT Endpoint: \n", self.__out)
                print("IN Endpoint: \n", self.__in)
                
            self.__dev.ctrl_transfer(0x40, 0x03, 0x04, 0x200)
            self.__dev.ctrl_transfer(0x40, 0x04, 0x08, 0x00, 0)
            self.__dev.ctrl_transfer(0x40, 0x09, 0xfa, 0x00, 0)
            self.__dev.ctrl_transfer(0x40, 0x02, 0x1311, 0x400, 0)
            self.__dev.ctrl_transfer(0x40, 0x06, 0x10a, 0x00, 0)
            self.__dev.ctrl_transfer(0x40, 0x00, 0x00, 0x00, 0)
            
        elif verbose:
            print("No USB devices was found")
            
    def close(self):
        usb.util.dispose_resources(self.__dev)
            
    def __read_buf(self):
        """ Read data from QWave USB into byte array """
        try:
            buf = self.__in.read(self.__in.wMaxPacketSize, 500)
        except Exception:
            return None

        return buf
            
    def __read_str(self):
        """ Read data from QWave USB and convert it to a string """
        """ If error will be empty string returned """
        buf = self.__read_buf()
        
        if (buf is not None) and (len(buf) > 4):
            result_str = ''.join([chr(x) for x in buf[2:-2]])
        else:
            result_str = ""
            
        return result_str
    
    def __read_response(self):
        """ Read tupel (errcode, data) from QWave """
        """ If error will be (0, "") tupel returned """
        resp = self.__read_str()
        ret_code = int(resp[0])
        
        if ret_code == CMD_RET_COMMAND_SUCCESS:
            if len(resp) > 2:
                return resp[2:]     # return resp without ret_code and space char
            else:
                return ""
        else:
            raise Exception(ExceptionDict[ret_code])
        
    def __qwave_cmd(self, command):
        """ Perform qwave ShortLink-protocol command and returns response """
        if self.__dev is not None:
            cmd_to_send = command + "\r\n"
            self.__out.write(cmd_to_send, len(cmd_to_send))
            return self.__read_response()
        
    def start_exposure(self):
        """ Starts taking a spectrum. Exact behavior depends on trigger mode (see below). Sets """
        """ status to either STATUS_WAITINGFORTRIGGER or STATUS_TAKING """
        return self.__qwave_cmd("ST")
    
    def cancel_exposure(self):
        """ Cancels taking a spectrum and sets status back to STATUS_IDLE """
        return self.__qwave_cmd("CL")
    
    def bye(self):
        """ Cancels taking a spectrum, sets status back to STATUS_IDLE and enters stand-by mode. """
        return self.__qwave_cmd("BYE")
    
    def get_status(self):
        """ Returns the status of the spectrometer as one of the following values:
                • STATUS_IDLE = 0
                • STATUS_WAITINGFORTRIGGER = 1
                • STATUS_TAKINGSPECTRUM = 2
                • STATUS_HASDATA = 3
                • STATUS_ERROR = -1 (not used so far) """
        return int(self.__qwave_cmd("S?"))
    
    def get_data(self):
        #return self.__qwave_cmd("R?")
        self.__out.write("R?\r\n", 4)
        return self.__read_buf()
    
    def get_device_id(self):
        """ Returns a string "ID" followed by Settings.DeviceID formatted as four digits """
        return self.__qwave_cmd("DI?")
            
    def get_manufacturer(self):
        """ Returns the string "RGB Photonics" """
        return self.__qwave_cmd("DM?")
    
    def get_description(self):
        """ Returns a string describing the devie depending on the device ID """
        return self.__qwave_cmd("DT?")
    
    def get_serial(self):
        """ Returns the serial number from Settings.SerialNo """
        return self.__qwave_cmd("DS?")
    
    def get_hardware_version(self):
        """ Returns the hardware version number formatted like "A.B.C". This value is taken from
            Settings.HardwareVersion, where Byte 2 corresponds to "A", byte 1 corresponds to "B"
            and byte 0 to "C". """
        return self.__qwave_cmd("DH?")
    
    def get_software_version(self):
        """ Returns the firmware version number formatted like "A.B.C" """
        return self.__qwave_cmd("DO?")
    
    def get_dev_max_value(self):
        """ Returns the maximum value that one pixel can have in a spectrum. This value can for
            example be used to determine if the CCD is overloaded and for auto-exposure. This value
            is taken from Settings. """
        return self.__qwave_cmd("DV?")
    
    def get_dev_pixelcount(self):
        """ The number of pixels in a spectrum that can actually be used (no dark or dummy pixels) """
        return self.__qwave_cmd("DP?")
    
    def get_dev_datacount(self):
        """ The total number of pixels in a spectrum including dark and dummy pixels """
        return self.__qwave_cmd("DC?")
    
    def get_first_darkpix(self):
        """ Returns the first dark pixel. Dark pixels are used to compensate the voltage offset """
        return self.__qwave_cmd("DDD?")
    
    def get_num_darkpix(self):
        """ Returns the number of dark pixels. Dark pixels are used to compensate the voltage offset """
        return self.__qwave_cmd("DDN?")
    
    def get_dev_firstpixel(self):
        """ The first pixel of the real spectrum (where PIXELCOUNT starts) """
        return self.__qwave_cmd("DDF?")
    
    def get_dev_mirror(self):
        """ Returns the value from Settings.MirrorSpectrum. 1 = the spectrum starts with the largest
            wavelength and must be mirrored. 0 = it starts with the lowest wavelength. """
        return self.__qwave_cmd("DR?")
    
    def get_sensortype(self):
        """ Returns the type of CCD sensor used from Settings.SensorType """
        stype = int(self.__qwave_cmd("DY?"))
        if stype == 1:
            return "TCD1304"
        elif stype == 2:
            return "TCD1254"
        elif stype == 3:
            return "ILX511"
        else:
            return "Unknown sensor type"
        
    def get_dev_resolution(self):
        """ Returns the spectrometer resolution (full width at half maximum) in 1/100 pixels from
            Settings.FWHM """
        return self.__qwave_cmd("DF?")
    
    def get_min_exposure_time(self):
        """ The minimum exposure time that can be set. Values are taken from Settings.MinExposureTime """
        return self.__qwave_cmd("LTN?")
    
    def get_max_exposure_time(self):
        """ The maximum exposure time that can be set. Values are taken from Settings.MaxExposureTime """
        return self.__qwave_cmd("LTP?")
    
    def get_calibrdata(self, page_num):
        """ Provides access to the non-volatile memory for storing calibration data. The memory
            consists of 127 pages with 1024 bytes each. For reading and writing a page, specify the
            page number. The structure of the data in this memory is determined by the host
            application """
        return self.__qwave_cmd("CDR={}".format(page_num))
    
    def set_calibrdata(self, page_num, data):
        """ Provides access to the non-volatile memory for storing calibration data. The memory
            consists of 127 pages with 1024 bytes each. For reading and writing a page, specify the
            page number. The structure of the data in this memory is determined by the host
            application """
        return self.__qwave_cmd("CDW={},{}".format(page_num, data))
    
    def reset(self):
        """ Resets the internal operation parameters, powers on all components (if in low power
            mode) and set the status to STATUS_IDLE. Does not reset any values that can be changed
            by the user. """
        return self.__qwave_cmd("RSTX")
        #self.__out.write("RES\r\n")
    
    def set_baudrate(self, baudrate=3000000):
        """ Changes the baud rate for serial communication. When the device is connected, it always
            starts with the default baud rate of 57600 Baud. You can then send this command to 
            increase or decrease the interface speed. The following baud rates are available: 
                300, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600, 3000000.
                
            After this command is sent, the return code 0 is returned using the old baud rate.
            Afterwards the device changes the interface speed and the next command to the device
            must then be sent using the new baud rate. """
            
        self.__dev.baudrate = baudrate
        return self.__qwave_cmd("BDR={}".format(baudrate))
    
    def get_exposure_time(self):
        """ Gets the exposure time (a.k.a. integration time) in microseconds. The timing is
            resetted and the new exposure period starts immediately. When connecting the device
            to USB, the exposure time is set to 0.01 sec. """
        return self.__qwave_cmd("T?")
    
    def set_exposure_time(self, time):
        """ Sets the exposure time (a.k.a. integration time) in microseconds. The timing is
            resetted and the new exposure period starts immediately. When connecting the device
            to USB, the exposure time is set to 0.01 sec. """
        return int(self.__qwave_cmd("T={}".format(time)))
    
    def get_temperature(self):
        """ Gets a value from which the current temperature of the CCD chip can be calculated """
        return int(self.__qwave_cmd("TMP?")) / 10
    
    def get_port_config(self):
        """ Sets and gets the configuration for each of the four I/O Pins. This is a 32-bit value, where
            byte 0 corresponds to pin 0, byte 1 to pin 1 and so on."""
        return self.__qwave_cmd("PC?")
    
    def set_port_config(self, config):
        """ Sets the configuration for each of the four I/O Pins. This is a 32-bit value, where
            byte 0 corresponds to pin 0, byte 1 to pin 1 and so on. Each of the four pins can be
            configured independently to one of the following functions:
                • 0 = OutputConstantLow (The I/O pin is constant at low level)
                • 1 = OutputConstantHigh (The I/O pin is constant at high level)
                • 2 = OutputDuringExpLow (The I/O pin is at low level during the exposure, otherwise high)
                • 3 = OutputDuringExpHigh (The I/O pin is at high level during the exposure, otherwise low)
                • 4 = Input (The I/O pin is used as an input: custom input or trigger source.)
                • 8 = OutputPulsed (The I/O pin is pulsed high and low to control an external xenon flash light.
                      Support for this feature is experimental and may be changed later on.)
                • 10 = OutputPulsedDuringExposure (The I/O pin is pulsed high and low during the exposure,
                       otherweise low. Support for this feature is experimental and may be changed later on) """
        return self.__qwave_cmd("PC={}".format(config))
    
    def port_read(self):
        """ Returns the actual current logic levels for all four I/O pins. This is a 32-bit value, where
            byte 0 corresponds to pin 0, byte 1 to pin 1 and so on."""
        return self.__qwave_cmd("P?")
    
    def get_trigger_config(self):
        """ Gets the trigger mode. """
        return self.__qwave_cmd("GC?")
    
    def set_trigger_config(self, config):
        """ Sets trigger mode. Byte 1 specifies the I/O pin used for external triggering
            (can be 0, 1, 2 or 3). Byte 0 specifies the trigger mode:
                • bit 0: external trigger on 0 = falling edge, 1 = rising edge
                • bit 1: 0 = trigger on end, 1 = trigger on start of exposure
                • bit 2: 0 = normal mode, 1 = low jitter mode (ignored if trigger on end)
                
            Bits 1 and 2 also affect software triggering (i.e. triggering by sending a START_EXPOSURE
            command). For more information, please see the chapter “Triggering and I/O port” in the
            spectrometer user manual. """
        return self.__qwave_cmd("GC={}".format(config))
    
    def set_trigger_enabled(self, enabled):
        """ Enables or disables the external trigger. If enabled, the device waits for a
            trigger event after START_EXPOSURE before taking the spectrum. """
        if enabled:
            return self.__qwave_cmd("G=1")
        else:
            return self.__qwave_cmd("G=0")
        
    def get_trigger_enabled(self):
        enabled = int(self.__qwave_cmd("G?"))
        if enabled == 1:
            return True
        else:
            return False

    
qwave = QWave(verbose = False)

print("Common device info\n==========================================\n")
print("Device ID: {}".format(qwave.get_device_id()))
print("Serial no.: {}".format(qwave.get_serial()))
print("Device description: {}".format(qwave.get_description()))
print("Hardware version: {}".format(qwave.get_hardware_version()))
print("Software version: {}".format(qwave.get_software_version()))
print("Manufacturer: {}".format(qwave.get_manufacturer()))

print("Device max value: {}".format(qwave.get_dev_max_value()))
print("Pixel count: {}".format(qwave.get_dev_pixelcount()))
print("Data count: {}".format(qwave.get_dev_datacount()))
print("First dark pixel: {}".format(qwave.get_first_darkpix()))
print("Number dark pixel: {}".format(qwave.get_num_darkpix()))
print("Device first pixel: {}".format(qwave.get_dev_firstpixel()))
print("Device mirror: {}".format(qwave.get_dev_mirror()))
print("Sensor type: {}".format(qwave.get_sensortype()))
print("Resolution: {}".format(qwave.get_dev_resolution()))
print("Min. exposure time: {}".format(qwave.get_min_exposure_time()))
print("Max. exposure time: {}".format(qwave.get_max_exposure_time()))

print("\nCurrent device settings\n==========================================\n")
print("Status: {}".format(qwave.get_status()))
print("Temperature: {} °C".format(qwave.get_temperature()))
print("Exposure time: {}".format(qwave.get_exposure_time()))
print("Port config: {}".format(qwave.get_port_config()))
print("Trigger config: {}".format(qwave.get_trigger_config()))
print("Trigger enabled: {}".format(qwave.get_trigger_enabled()))

print("\nReading spectrum...\n")
qwave.reset()
#qwave.start_exposure()
#for i in range(3):
#    status = qwave.get_status()
#    print("Data {}. Status: {}".format(i + 1, status))
#    print(qwave.get_data())
    
#qwave.cancel_exposure()

qwave.close()
