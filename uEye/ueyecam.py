# -*- coding: utf-8 -*-
from time import gmtime, strftime, sleep
from pyueye import ueye
import numpy as np
import cv2

class uEyeCam:
    ''' uEye camera class '''
    def __init__(self):
        self.__is_open = False
        self.__is_video_on = False
        
    def camera_list(self):
        ''' Returns the list of available cameras '''
        result = []

        cam_count = ueye.INT(8)
        nRet = ueye.is_GetNumberOfCameras(cam_count)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error by getting list of cameras")
        else:
            for i in range(int(cam_count)):
                hCam = ueye.HIDS(i)
                nRet = ueye.is_InitCamera(hCam, None)
                if nRet != ueye.IS_SUCCESS:
                    raise Exception("Error camera initialization")
                else:
                    cInfo = ueye.CAMINFO()
                    nRet = ueye.is_GetCameraInfo(hCam, cInfo)
                    if nRet != ueye.IS_SUCCESS:
                        raise Exception("Error reading camera information")
                    else:
                        result.append(cInfo.SerNo.decode('utf-8'))
                    
                    ueye.is_ExitCamera(hCam)

        return result
        
    def open(self, index):
        ''' Open and initialize camera '''
        self.__hCam             =   ueye.HIDS(index)
        self.__sInfo            =   ueye.SENSORINFO()
        self.__cInfo            =   ueye.CAMINFO()
        self.__MemID            =   ueye.int()
        self.__rectAOI          =   ueye.IS_RECT()
        self.__pitch            =   ueye.INT()
        self.__nBitsPerPixel    =   ueye.INT(24)
        self.__channels         =   3
        self.__m_nColorMode     =   ueye.INT()
        self.__bytes_per_pixel  =   int(self.__nBitsPerPixel / 8)
        
        nRet = ueye.is_InitCamera(self.__hCam, None)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error camera initialization")

        nRet = ueye.is_GetCameraInfo(self.__hCam, self.__cInfo)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error by getting camera info")

        nRet = ueye.is_GetSensorInfo(self.__hCam, self.__sInfo)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error by getting sensor info")

        nRet = ueye.is_ResetToDefault(self.__hCam)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error resetting camera")

        nRet = ueye.is_SetDisplayMode(self.__hCam, ueye.IS_SET_DM_DIB)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error setting display mode")
            
        nRet = ueye.is_SetColorMode(self.__hCam, ueye.IS_COLORMODE_BAYER)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error setting color mode")
        
        if int.from_bytes(self.__sInfo.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_BAYER:
            ueye.is_GetColorDepth(self.__hCam, self.__nBitsPerPixel, self.__m_nColorMode)
            self.__color_mode = 'Bayer'
        elif int.from_bytes(self.__sInfo.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_CBYCRY:
            self.__m_nColorMode = ueye.IS_CM_BGRA8_PACKED
            self.__nBitsPerPixel = ueye.INT(32)
            self.__color_mode = 'CBYCRY'
        elif int.from_bytes(self.__sInfo.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_MONOCHROME:
            self.__m_nColorMode = ueye.IS_CM_MONO8
            self.__nBitsPerPixel = ueye.INT(8)
            self.__color_mode = 'Monochrome'
        else:
            self.__m_nColorMode = ueye.IS_CM_MONO8
            self.__nBitsPerPixel = ueye.INT(8)
            self.__color_mode = 'Mono8'
        
        self.__bytes_per_pixel = int(self.__nBitsPerPixel / 8)

        nRet = ueye.is_AOI(self.__hCam, ueye.IS_AOI_IMAGE_GET_AOI, self.__rectAOI, ueye.sizeof(self.__rectAOI))
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error by reading AOI")
        self.__width = self.__rectAOI.s32Width
        self.__height = self.__rectAOI.s32Height
        
        self.__is_open = True
        
        print("Camera successfully initialized")
        
    def close(self):
        ''' Close camera '''
        if self.__is_open:
            ueye.is_ExitCamera(self.__hCam)
            self.__is_open = False
        
    def is_open(self):
        ''' Returns true if camera is open, otherwise false '''
        return self.__is_open
    
    def get_color_mode(self):
        ''' Returns the current color mode '''
        return self.__color_mode
    
    def get_serial(self):
        ''' Returns the serial number of a camera '''
        return self.__cInfo.SerNo.decode('utf-8')
    
    def get_bpp(self):
        ''' Returns the "bits per pixel" value '''
        return int(self.__nBitsPerPixel)
    
    def get_sensor_name(self):
        ''' Returns the sensor name '''
        return self.__sInfo.strSensorName.decode('utf-8')
    
    def get_AOI(self):
        ''' Returns area of interest (AOI) '''
        return self.__rectAOI
            
    def set_AOI(self, x, y, width, height):
        ''' Setup area of interest (AOI) '''
        rectAOI = ueye.IS_RECT()
        rectAOI.s32X = x
        rectAOI.s32Y = y
        rectAOI.s32Width = width
        rectAOI.s32Height = height
        
        if rectAOI.s32X + rectAOI.s32Width > self.__width:
            rectAOI.s32Width = self.__width - rectAOI.s32X
            
        if rectAOI.s32Y + rectAOI.s32Height > self.__height:
            rectAOI.s32Height = self.__height - rectAOI.s32Y
        
        start_needed = False
        if self.__is_video_on:
            self.stop_video()
            start_needed = True
        
        nRet = ueye.is_AOI(self.__hCam, ueye.IS_AOI_IMAGE_SET_AOI, rectAOI, ueye.sizeof(rectAOI))
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error by setting AOI")
        else:
            self.__rectAOI = rectAOI
            self.__width = self.__rectAOI.s32Width
            self.__height = self.__rectAOI.s32Height
            
        if start_needed:
            self.start_video()
            
    def set_whitebalance(self, whitebalance):
        ''' Set whitebalance '''
        if whitebalance == 0:
            enable = ueye.double(1)
            ueye.is_SetAutoParameter(self.__hCam, ueye.IS_SET_ENABLE_AUTO_WHITEBALANCE, enable, ueye.double(0))
        else:
            enable = ueye.double(0)
            ueye.is_SetAutoParameter(self.__hCam, ueye.IS_SET_ENABLE_AUTO_WHITEBALANCE, enable, ueye.double(0))
            
    def get_exposure(self):
        ''' Returns the exposure as a tupel (min_value, max_value) '''
        minexp = ueye.double()
        maxexp = ueye.double()

        nRet = ueye.is_Exposure(self.__hCam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MIN, minexp, ueye.sizeof(ueye.double()))
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error reading exposure from camera")
            
        nRet = ueye.is_Exposure(self.__hCam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MAX, maxexp, ueye.sizeof(ueye.double()))
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error reading exposure from camera")
        
        return (int(minexp * 1000), int(maxexp * 1000))
        
    def set_exposure(self, value):
        ''' Set camera exposure '''
        nRet = ueye.is_Exposure(self.__hCam, ueye.IS_EXPOSURE_CMD_SET_EXPOSURE, ueye.double(value / 1000), ueye.sizeof(ueye.double()))
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error setting exposure")

    def start_video(self):
        """ Allocates an image memory for an image having its dimensions
            defined by width and height and its color depth defined
            by nBitsPerPixel """
        self.__pcImageMemory = ueye.c_mem_p()
        
        nRet = ueye.is_AllocImageMem(self.__hCam, self.__width, self.__height, self.__nBitsPerPixel, self.__pcImageMemory, self.__MemID)
        if nRet != ueye.IS_SUCCESS:
            raise Exception('Error allocating image memory')
        else:
            nRet = ueye.is_SetImageMem(self.__hCam, self.__pcImageMemory, self.__MemID)
            if nRet != ueye.IS_SUCCESS:
                raise Exception("Set image memory error")
            else:
                nRet = ueye.is_SetColorMode(self.__hCam, self.__m_nColorMode)
                
        nRet = ueye.is_CaptureVideo(self.__hCam, ueye.IS_DONT_WAIT)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error starting video capture")

        nRet = ueye.is_InquireImageMem(self.__hCam, self.__pcImageMemory, self.__MemID, self.__width, self.__height, self.__nBitsPerPixel, self.__pitch)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("Error inquire image memory")
            
        self.__is_video_on = True

    def stop_video(self):
        """ Releases an image memory that was allocated using is_AllocImageMem()
            and removes it from the driver management """
        if self.__pcImageMemory is not None:
            ueye.is_StopLiveVideo(self.__hCam, ueye.IS_FORCE_VIDEO_STOP)
            sleep(0.2)
            
            ueye.is_ClearSequence(self.__hCam)
            ueye.is_FreeImageMem(self.__hCam, self.__pcImageMemory, self.__MemID)
            self.__pcImageMemory = None
            
        self.__is_video_on = False

    def save_image(self, filename, add_time = False, display = True):
        """ Get image from camera and save it to the file """
        array = ueye.get_data(self.__pcImageMemory, self.__width, self.__height, self.__nBitsPerPixel, self.__pitch, copy = False)
        frame = np.reshape(array, (self.__height.value, self.__width.value, self.__bytes_per_pixel))
    
        if add_time:
            cv2.imwrite(strftime("%H_%M_%S_", gmtime()) + filename, frame);
        else:
            cv2.imwrite(filename, frame);
            
        if display:
            frame = cv2.resize(frame, (0,0), fx=0.25, fy=0.25)
            cv2.imshow("uEye camera", frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                return

    def get_image(self, scale = 0.25):
        """ Returns the image as an array """
        img = ueye.get_data(self.__pcImageMemory, self.__width, self.__height, self.__nBitsPerPixel, self.__pitch, copy = False)
        return np.reshape(img, (self.__height.value, self.__width.value, self.__bytes_per_pixel))
