# -*- coding: utf-8 -*-
from PyQt5 import uic, QtWidgets

from time import sleep
import cv2
import sys

from ueyecam import uEyeCam

class uEyeApp:
    ''' Qt app to control uEye camera '''
    def __init__(self):
        self.__runcam = False
        
        self.__camera = uEyeCam()
        
        self.__app = QtWidgets.QApplication(sys.argv)
        self.__app.aboutToQuit.connect(self.__app_closed)
        
        self.__win = uic.loadUi('options.ui')
        self.__win.setWindowTitle('uEye camera')

        # Connect signals
        self.__win.btnClose.clicked.connect(self.__on_close_clicked)
        self.__win.btnStartCamera.clicked.connect(self.__on_start_clicked)
        self.__win.btnStopCamera.clicked.connect(self.__on_stop_clicked)
        self.__win.btnSaveAOI.clicked.connect(self.__on_save_AOI_clicked)
        self.__win.edAOIX.valueChanged.connect(self.__on_AOI_changed)
        self.__win.edAOIY.valueChanged.connect(self.__on_AOI_changed)
        self.__win.edAOIWidth.valueChanged.connect(self.__on_AOI_changed)
        self.__win.edAOIHeight.valueChanged.connect(self.__on_AOI_changed)
        self.__win.slExposure.valueChanged.connect(self.__on_exposure_changed)

    def __app_closed(self):
        ''' Window close event: close camera and release resources '''
        self.__on_stop_clicked();
        self.__camera.close()
        
    def start(self):
        ''' Display app window '''
        self.__win.cbCamera.clear()
        
        cameras = self.camera_list()
        
        for camera in cameras:
            self.__win.cbCamera.addItem(camera)
        self.__win.cbCamera.currentIndexChanged.connect(self.__on_camera_selected)
        
        if len(cameras) > 0:
            self.__camera.open(0)
            self.__update_ui()
        
        self.__win.show()
        self.__app.exec()
        
    def open_camera(self, camera_index):
        ''' Open and initialize camera with a given index '''
        self.__camera.open(camera_index)
        
    def close_camera(self):
        ''' Close camera and release resources '''
        self.__camera.close()
        
    def start_camera(self):
        ''' Start live video '''
        self.__camera.start_video()
        
    def stop_camera(self):
        ''' Stop live video '''
        self.__camera.stop_video()
        
    def save_image(self, file_name, add_time):
        ''' Save camera frame into a file '''
        self.__camera.save_image(file_name, add_time=add_time, display=False)
        
    def __update_ui(self):
        ''' Update UI information '''
        self.__win.lbModel.setText(self.__camera.get_sensor_name())
        self.__win.lbSerial.setText(self.__camera.get_serial())
        self.__win.lbColorMode.setText(self.__camera.get_color_mode())
        self.__win.lbBitsPerPixel.setText(str(self.__camera.get_bpp()))

        aoi = self.__camera.get_AOI()
        self.__win.edAOIX.setValue(aoi.s32X)
        self.__win.edAOIY.setValue(aoi.s32Y)
        self.__win.edAOIWidth.setValue(aoi.s32Width)
        self.__win.edAOIHeight.setValue(aoi.s32Height)
        self.__win.lbImageSize.setText(f'{aoi.s32Width}x{aoi.s32Height}')
        self.__win.btnSaveAOI.setEnabled(False)

        minexp, maxexp = self.__camera.get_exposure()
        self.__win.slExposure.setMinimum(minexp)
        self.__win.slExposure.setMaximum(maxexp)
        self.__win.slExposure.setValue((maxexp - minexp) / 2)

    def camera_list(self):
        ''' Returns list of connected cameras '''
        return self.__camera.camera_list()
    
    def __on_camera_selected(self, index):
        ''' Camera selected '''
        self.__camera.stop_video()
        self.__camera.close()
        
        self.__camera.open(index)
        self.__update_ui()
        
    def __on_exposure_changed(self, value):
        ''' Exposure slider changed '''
        if self.__camera.is_open():
            self.__camera.set_exposure(value)
        
    def __on_start_clicked(self):
        ''' Start camera video '''
        self.__win.btnStartCamera.setEnabled(False)
        self.__win.btnStopCamera.setEnabled(True)
        
        try:
            if self.__camera is not None:
                self.__camera.start_video()
                self.__runcam = True
                while self.__runcam:
                    frame = self.__camera.get_image()
                    cv2.imshow("uEye camera view", cv2.resize(frame, (0,0), fx = 0.25, fy = 0.25))
                    cv2.waitKey(1)
                    sleep(0.2)
        except Exception:
            self.__win.btnStartCamera.setEnabled(True)
            self.__win.btnStopCamera.setEnabled(False)

    def __on_stop_clicked(self):
        ''' Stop camera video '''
        if self.__runcam == True:
            self.__runcam = False
        
            sleep(0.2)
            self.__camera.stop_video()
            
            cv2.destroyAllWindows()
            
        self.__win.btnStartCamera.setEnabled(True)
        self.__win.btnStopCamera.setEnabled(False)
        
    def __on_AOI_changed(self):
        ''' Enable "Save AOI" button by changing any AOI value '''
        self.__win.btnSaveAOI.setEnabled(True)
            
    def __on_save_AOI_clicked(self):
        ''' Setup AOI '''
        self.__win.btnSaveAOI.setEnabled(False)
        self.__camera.set_AOI(self.__win.edAOIX.value(), self.__win.edAOIY.value(), self.__win.edAOIWidth.value(), self.__win.edAOIHeight.value())
        
    def __on_close_clicked(self):
        ''' Close app '''
        self.__app.closeAllWindows()

if __name__ == '__main__':
    ueyecam = uEyeApp()
    ueyecam.start()
