# -*- coding: utf-8 -*-
from ueyeapp import uEyeApp

class uEye:
    def __init__(self):
        ''' Initialize uEye modul '''
        self.__app = uEyeApp()
        
    def options(self):
        ''' Display GUI window '''
        self.__app.start()
        
    def camera_list(self):
        ''' Returns list of available cameras '''
        return self.__app.camera_list()
        
    def open_camera(self, camera_index):
        ''' Connect to the camera '''
        self.__app.open_camera(camera_index)
        
    def close_camera(self):
        ''' Close connection '''
        self.__app.close_camera()
        
    def start_video(self):
        ''' Start live video '''
        self.__app.start_camera()
        
    def stop_video(self):
        ''' Stop live video '''
        self.__app.stop_camera()
        
    def save_image(self, filename, add_time):
        ''' Save image into the file '''
        self.__app.save_image(filename, add_time)
        
ueyecam = uEye()
