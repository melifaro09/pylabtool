from steinmeierapp import SteinmeierApp

class Steinmeier:
    ''' Modul for Steinmeier's movable table '''
    def __init__(self):
        ''' Initialize modul '''
        self.__app = SteinmeierApp()
            
    def options(self):
        self.__app.start()
        
    def open(self, index):
        ''' Connect to the steinmeier table
            @param index - COM port index
            '''
        pass
    
    def close(self):
        ''' Close connection '''
        pass

steinmeier = Steinmeier()
