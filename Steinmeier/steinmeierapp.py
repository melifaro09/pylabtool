# -*- coding: utf-8 -*-
from PyQt5 import uic, QtWidgets
import sys
import time

import serial
import serial.tools.list_ports

class SteinmeierApp:
    ''' Qt app to control uEye camera '''
    def __init__(self):
        self.__app = QtWidgets.QApplication(sys.argv)
        self.__app.aboutToQuit.connect(self.__app_closed)
        
        self.__win = uic.loadUi('options.ui')
        self.__win.setWindowTitle('Steinmeier step table')

        # Connect signals
        self.__win.btnConnect.clicked.connect(self.__on_open_clicked)
        self.__win.btnDisconnect.clicked.connect(self.__on_close_clicked)
        self.__win.btnMoveAbs.clicked.connect(self.__on_move_abs_clicked)
        self.__win.btnMoveRel.clicked.connect(self.__on_move_rel_clicked)
        self.__win.btnSend.clicked.connect(self.__on_send_cmd_clicked)
        self.__win.btnClearOut.clicked.connect(self.__on_clear_output_clicked)
        self.__win.btnHome.clicked.connect(self.__on_home_clicked)
        
        self.__init_components()
        
    def __init_components(self):
        ''' Initialize UI elements '''
        self.__com_list = serial.tools.list_ports.comports()
        for port, desc, hwid in self.__com_list:
            self.__win.cbPort.addItem(f'{port}: {desc}')
            
    def __enable_controls(self, enable):
        ''' Enable/disable GUI elements '''
        self.__win.edPositionAbs.setEnabled(enable)
        self.__win.btnMoveAbs.setEnabled(enable)
        self.__win.edPositionRel.setEnabled(enable)
        self.__win.btnMoveRel.setEnabled(enable)
        self.__win.edCommand.setEnabled(enable)
        self.__win.btnSend.setEnabled(enable)
        self.__win.edOutput.setEnabled(enable)
        self.__win.btnClearOut.setEnabled(enable)
        self.__win.btnHome.setEnabled(enable)

    def __app_closed(self):
        ''' Closing app '''
        pass
        
    def start(self):
        ''' Display app window '''
        self.__win.show()
        self.__app.exec()
        
    def port_list(self):
        return ...
        
    def __on_open_clicked(self):
        ''' Connect to the Steinmeiers table '''
        try:
            self.__port = self.__com_list[self.__win.cbPort.currentIndex()].device
            self.__serial = serial.Serial(self.__port, baudrate=38400)
        
            self.__win.btnConnect.setEnabled(False)
            self.__win.btnDisconnect.setEnabled(True)
            
            self.init_controller()
            
            self.__enable_controls(True)
        except Exception as err:
            print(f'Error by open port: {err}')
            self.__enable_controls(False)
        
        print(f'Connect to {self.__port}')
        
    def __on_close_clicked(self):
        ''' Disconnect from controller '''
        self.__serial.close()
        
        self.__win.btnConnect.setEnabled(True)
        self.__win.btnDisconnect.setEnabled(False)
        self.__enable_controls(False)
        #self.__app.closeAllWindows()
    
    def __on_move_abs_clicked(self):
        ''' Move to absolute position '''
        pass
    
    def __on_move_rel_clicked(self):
        ''' Move to relative position '''
        pass
    
    def __on_send_cmd_clicked(self):
        ''' Send custom command to the controller '''
        pass
    
    def __on_clear_output_clicked(self):
        ''' Clear output '''
        pass
    
    def __on_home_clicked(self):
        ''' Perform homing '''
        self.__homing()
    
    def __wait_move(self):
        ''' Wait while moving is done '''
        self.__update_status()
        while self.__is_running:
            self.__update_status()
            time.sleep(0.02)
    
    def __check_run(self):
        ''' Check controller status and wait if needed or perform homing '''
        if not self.__is_ref_set:
            self.__homing()

            while not self.__is_ref_set:
                time.sleep(0.02)
                self.__update_status()

                if not self.__is_running and not self.__is_ref_set:
                    self.__homing();

        self.__wait_move()

    def __serial_receive(self):
        ''' Receive string from serial connection '''
        print("Receive data...")
        out = ""
        
        while self.__serial.inWaiting():
            out = out + str(self.__serial.readline())
            if '-->' in out:
                break
        print(f'Received string: {out}')
        return out
    
    def __send_cmd(self, cmd, checksum=False):
        ''' Send command to the controller '''
        #self.__serial.write(cmd + '\r')
        
        try:
            if checksum:
                ch = 0
                for c in cmd:
                    ch = (ch + ord(c)) & 0xFF

                hexval = hex(ch)[-2:]
                
                if ord(hexval[-2]) < 0x30 or ord(hexval[-2]) > 0x39:
                    cmd += chr((0x3A + (ord(hexval[-2]) - ord('a'))) & 0xFF)
                else:
                    cmd += hexval[-2]
                    
                if ord(hexval[-1]) < 0x30 or ord(hexval[-1]) > 0x39:
                    cmd += chr((0x3A + (ord(hexval[-1]) - ord('a'))) & 0xFF)
                else:
                    cmd += hexval[-1]
                
                print(f'Command with a checksum: {cmd}')
            else:
                print(f'Command: {cmd}')
                
            cmd = cmd + '\r'

            self.__serial.write(str.encode(cmd))

            print("Ok, receive answer...")
            return self.__serial_receive()

        except Exception as e:
            print(e)
            
    def __set_order_x(self, orderNum, pos, speed, start_accel, stop_accel):
        ''' Set order for 0 (x) axe '''
        self.__select_axis(0)
        self.__set_order(orderNum, (int)(pos * 1000), speed, start_accel, stop_accel, 0, 0, -1, 0)
        
    def __set_order_y(self, orderNum, pos, speed, start_accel, stop_accel):
        ''' Set order for 1 (y) axe '''
        self.__select_axis(1)
        self.__set_order(orderNum, (int)(pos * 1000), speed, start_accel, stop_accel, 0, 0, -1, 0)
	
    def __set_order_xy(self, orderNum, x, y, speed, start_accel, stop_accel):
        self.__set_order_x(orderNum, x, speed, start_accel, stop_accel)
        self.__set_order_y(orderNum, y, speed, start_accel, stop_accel);

    def __move_abs(self, axis, position, speed, start_acc, stop_acc):
        ''' Move to a given position '''
        self.__select_axis(axis)
        self.__set_order(1, position * 1000, speed, start_acc, stop_acc, 0, 0, -1, 0)
        self.__run_order_and_wait(axis, 1)

    def __run_order_and_wait(self, axis, orderNum):
        self.__select_axis(axis)

        self.__clear_errors()
        self.__updateStatus()
        self.__checkRun()
		
        self.__send_cmd(f'MOVE {orderNum}', True)
		
        self.__wait_move()
     
    def __set_order(self, orderNum, position, speed, start_accel, stop_accel, profil, speed_profil, next_order, delay):
        ''' Write order into controller '''
        control_word = 0x2000;
        
        if profil == 0:
            control_word = 0x2000
        elif profil == 1:
            control_word = 0x2200
        elif profil == 2:
            control_word = 0x12000
            
        if next_order != -1 and next_order != 0:
            control_word += 0x0008
        else:
            next_order = 0
		
        self.__send_cmd(f'ORDER {orderNum} {position} {speed} {control_word} {start_accel} {stop_accel} {speed_profil} -1 {next_order} {delay}', True)
            
    def __select_axis(self, axis):
        ''' Select axis (0..1) '''
        self.__send_cmd(f'\\ {axis}', True)
        
    def __update_status(self):
        ''' Update status variables '''
        status = self.__send_cmd("STATUS", True).split('\r\n')[1].strip()
        print(f'Status: {status}')
     
        status_text = status[len(status) - 6, len(status) - 2]
        stat = int(status_text, 16)
	
        self.__is_running       =   (stat & 0x01) == 0x01
        self.__is_ref_set       =   (stat & 0x02) == 0x02
        self.__is_in_home       =   (stat & 0x04) == 0x04
        self.__is_in_position   =   (stat & 0x08) == 0x08
        self.__is_ref_run       =   (stat & 0x20) == 0x20
        
    def __homing(self):
        ''' Clear all errors and perform homing '''
        self.__clear_errors()
        self.__updateStatus()
				
        while not self.__is_ref_set:
            if not self.__is_running:
                self.__send_cmd("EN", True)
                self.__send_cmd("RS232T 5000", True)
                self.__send_cmd("ACTRS232 2", True)
                self.__send_cmd("MH", True)
			
            self.__updateStatus();
            time.sleep(0.2)
        
    def init_controller(self):
        ''' Initialize controller '''
        self.__send_cmd('\\ 0', False)
        self.__send_cmd('PROMPT 3', True)
        self.__send_cmd('\\ 1', True)
        self.__send_cmd('PROMPT 3', False)
        self.__send_cmd('SCAN', True)
        self.__send_cmd('MSG 0', True)
        
    def __clear_errors(self):
        ''' Clear controller errors '''
        print("Clear errors")
        if 'no error' not in self.__send_cmd('ERRCODE', True):
            self.__send_cmd('CLRFAULT', True)

if __name__ == '__main__':
    steinmeier = SteinmeierApp()
    steinmeier.start()
