import clr

clr.AddReference("Thorlabs.MotionControl.DeviceManagerCLI")
clr.AddReference("Thorlabs.MotionControl.KCube.PositionAlignerCLI")

from Thorlabs.MotionControl.DeviceManagerCLI import DeviceManagerCLI
from Thorlabs.MotionControl.KCube.PositionAlignerCLI import KCubePositionAligner

from time import sleep

class Kinesis_KPA101():
    ''' Class to control Thorlabs KPA101 Kube '''

    def __init__(self):
        ''' Initialization: get device list '''
        DeviceManagerCLI.BuildDeviceList()
        self.__dev_list = DeviceManagerCLI.GetDeviceList(69)
        print("Initialization finished")
        
    def list(self):
        ''' Returns list of available devices '''
        return self.__dev_list
            
    def open(self, index):
        ''' Initialize device '''
        self.__serial = self.__dev_list[index]

        self.__dev = KCubePositionAligner.CreateKCubePositionAligner(self.__serial)
        self.__dev.Connect(self.__serial)
        
        if not self.__dev.IsSettingsInitialized():
            self.__dev.WaitForSettingsInitialized(5000)
        
        self.__dev.StartPolling(250)
        sleep(0.005)

        self.__dev.EnableDevice()    
        sleep(0.005)
    
        self.__dev.LoadMotorConfiguration(self.__serial)
        print("KPA101 connected")
        
    def get_coords(self):
        pos = self.__dev.GetDemandedPosition()
        print(pos.X, pos.Y, sep=', ', flush=True)
        
    def info(self):
        return self.__dev.getDeviceInfo()
        
    def close(self):
        ''' Close connection with a device '''
        self.__dev.StopPolling()
        self.__dev.DisableDevice()
        self.__dev.Disconnect(True)
        print("KPA101 closed")

kpa101 = Kinesis_KPA101()
