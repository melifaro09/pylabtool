import clr

clr.AddReference("Thorlabs.MotionControl.DeviceManagerCLI")
clr.AddReference("Thorlabs.MotionControl.KCube.BrushlessMotorCLI")
clr.AddReference("System")

from System import Decimal
from Thorlabs.MotionControl.DeviceManagerCLI import DeviceManagerCLI
from Thorlabs.MotionControl.KCube.BrushlessMotorCLI import KCubeBrushlessMotor

from time import sleep

class Kinesis_KBD101():
    ''' Class to control Thorlabs KBD101 Kube '''
    
    def __init__(self):
        ''' Initialization: get device list '''
        DeviceManagerCLI.BuildDeviceList()
        self.__dev_list = DeviceManagerCLI.GetDeviceList(28)
        print("Initialization finished")
        
    def options(self):
        pass

    def list(self):
        ''' Returns list of available devices '''
        return self.__dev_list
		
    def home(self):
        self.__dev.Home(60000)
        print("Homing finished")
            
    def open(self, index):
        ''' Initialize device '''
        if hasattr(self, '__dev'):
            self.close()

        self.__serial = str(self.__dev_list[index])

        self.__dev = KCubeBrushlessMotor.CreateKCubeBrushlessMotor(self.__serial)
        self.__dev.Connect(self.__serial)
        
        if not self.__dev.IsSettingsInitialized():
            self.__dev.WaitForSettingsInitialized(5000)
        
        self.__dev.StartPolling(250)
        sleep(0.05)

        self.__dev.EnableDevice()
        sleep(0.05)
    
        self.__dev.LoadMotorConfiguration(self.__serial);
        sleep(0.05)
        
        self.__axis_param = self.__dev.GetStageAxisParams()
        self.__axis_min_pos = Decimal.ToInt32(self.__axis_param.MinPosition)
        self.__axis_max_pos = Decimal.ToInt32(self.__axis_param.MaxPosition)
        
        print("Position range: {} - {}".format(self.__axis_param.MinPosition, self.__axis_param.MaxPosition))
        print("Velocity range: {} - {}".format(0, self.__axis_param.MaxVelocity))
        print("Acceleration range: {} - {}".format(0, self.__axis_param.MaxAcceleration))

        print("KBD101 connected")
        
    def velocity(self, speed):
        vel = self.__dev.GetVelocityParams()

        speed = max(0, speed)
        speed = min(1, speed)
        
        vel.MaxVelocity = Decimal(speed * Decimal.ToInt32(self.__axis_param.MaxVelocity))

        self.__dev.SetVelocityParams(vel)
        print("KBD101 velocity was set")
        
    def move_abs(self, position):
        ''' Move motor to absolute position '''
        position = max(self.__axis_min_pos, position)
        position = min(self.__axis_max_pos, position)

        self.__dev.MoveTo(Decimal(position), 60000)
        
        print("Position: {}".format(self.__dev.TargetPosition))
        
        print("KBD101 moving completed")

    def move_rel(self, distance):
        ''' Move motor to relative distance '''
        new_pos = Decimal.ToInt32(self.__dev.Position) + distance;
        
        new_pos = max(self.__axis_min_pos, new_pos)
        new_pos = min(self.__axis_max_pos, new_pos)
        
        self.__dev.MoveTo(Decimal(new_pos), 60000)
        
        #self.__dev.SetMoveRelativeDistance(Decimal(distance))
        #self.__dev.MoveRelative(60000)
        print("KBD101 moving completed")
        
    def close(self):
        ''' Close connection with a device '''
        self.__dev.StopPolling();
        self.__dev.Disconnect(True);
        print("KBD101 closed")
        
    def check_homing(self):
        if self.__dev.NeedsHoming and self.__dev.CanHome:
            print("Homing is needed")
        else:
            print("No homing is needed")

kbd101 = Kinesis_KBD101()
